<!doctype html>
<html class="no-js" lang="">
    <head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117334883-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117334883-1');
</script>


        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Avios Thought Leadership</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="refresh" content="1;URL=download.php">
        <link rel="stylesheet" href="styles/styles.css?ver=20180319_5">

        <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="icon" type="image/png" href="favicon.ico" />
        
    </head>
    <body>

        <div id="cookie-message">
            <p>Our website works best using cookies. By continuing to use the site, you agree to the use of cookies. Read more about our <a href="https://aviosgroup.com/uk/en/cookie-policy" class="u-text--white" target="_blank">Cookies</a> and <a href="https://aviosgroup.com/uk/en/privacy-policy" class="u-text--white" target="_blank">Privacy</a> notice.            
            <button id="cookie-close" class="cookie-close" data-js="true"><span class="visuallyhidden"> Accept</span></button>
        </div>

        
        <header class="header" id="header">
          <nav class="nav">
            
            <div class="mobileLogo">
                <a href="http://aviosgroup.com" target="_blank">
                    <img src="images/avios-logo.png" class="logo" alt="avios logo" />
                </a>
            </div>
            
            <div class="container hiddenNavContainer">
                
                    <ul>
                        <li>
                            <a href="index.php#customer-loyalty" class="">Value of customer loyalty</a>
                        </li>
                        <li class="borderRight">
                            <a href="index.php#loyalty-by-numbers" class="">Loyalty by numbers</a>
                        </li>
                        <li class="noBorder hiddenMobile">
                            <a href="http://aviosgroup.com" target="_blank">
                                <img src="images/avios-logo.png" class="logo" alt="avios logo" />
                            </a>
                        </li>
                        <li>
                            <a href="index.php#increase-returns" class="">Increase returns</a>
                        </li>
                        <li>
                            <a href="index.php#section-whitepaper" class="">Whitepaper</a>
                        </li>
                    </ul>
                
            </div>
            
            <div id="nav-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </nav>                     
          
      </header>



      <section id="whitepaper-form" class="paddingThanks">
       
        <div class="js-slideIn-onsuccess show">
            <p class="success-title">Thank you for downloading our whitepaper</p>
            <p>To discover how Avios improves loyalty ROI by sector, choose from the following case studies:</p>
            <ul class="case-studies">
                <li>
                    <a href="https://aviosgroup.com/Avios_B2B_Hilton_Case_Study.pdf" onClick="ga('send', 'event', { eventCategory: 'PDF Download', eventAction: 'Download', eventLabel: 'Hilton_PDF_Download'});"><span><img src="images/hilton.jpg" alt="Hilton case study" /></span>Hilton</a>
                </li>
                <li>
                     <a href="https://aviosgroup.com/Avios_B2B_Kaligo_Case_Study.pdf" onClick="ga('send', 'event', { eventCategory: 'PDF Download', eventAction: 'Download', eventLabel: 'Kaligo_PDF_Download'});"><span><img src="images/kaligo.jpg" alt="Hilton case study" /></span>Kaligo</a>
                </li>                    
                <li>
                    <a href="download/Avios_B2B_Marketing_Bicester_VillageCS_Final_A4_HR.pdf" onClick="ga('send', 'event', { eventCategory: 'PDF Download', eventAction: 'Download', eventLabel: 'Bicester_Village_Shopping_Collection_PDF_Download'});"><span><img src="images/Bicester.jpg" alt=" Bicester Village Shopping Collection" /></span> Bicester Village Shopping Collection</a>
                </li>
            </ul>
            
            <p>To discuss how loyalty can maximise returns for your business, contact your Partnerships &amp; Business Development Manager:</p>
            
            <div class="contact-details">
                <p>Tel. : +44 (0) 7787 295 265  </p>
                <p>Email : <a href="mailto:ross.mitchell@avios.com">ross.mitchell@avios.com</a></p>
            </div>
            

            <a class="hidden" id="pdf-download" title="hello"></a>
        </div>

      </section>

      <section id="partnership-cta">
        <div class="container">
          <p class="section-title">Become an Avios partner</p>
          <p>We welcome enquiries from businesses interested in partnering with Avios. Please get in touch to see how we could work together to drive loyalty from your current and future customers.</p>
          <a href="mailto:partners@avios.com" target="_top">partners@avios.com</a> 
        </div>          
      </section>

      <section id="social-bar">
          <p>Connect with us</p>

          <a href="https://www.linkedin.com/company/avios" target="_blank"><img src="images/icons/linkedin.png" alt="Linkedin" /></a>
          <a href="https://www.youtube.com/user/AviosGlobal" target="_blank"><img src="images/icons/youtube.png" alt="youtube" /></a>
      </section>

      <footer>

        <div class="footer-logo">
                <a href="#">
                    <img src="images/avios-logo.png" class="logo" alt="avios logo" />
                </a>
        </div>
          
          <nav id="footer-nav">
              <ul>
                  <li>
                      <a href="https://www.aviosgroup.com/uk/en/privacy-policy">Privacy Policy</a>
                  </li>
                  <li>
                      <a href="https://www.aviosgroup.com/uk/en/accessibility">Accessibility</a>
                  </li>
                  <li>
                      <a href="https://aviosgroup.com/uk/en/terms">Terms &amp; conditions</a>
                  </li>
                  <li>
                      <a href="https://aviosgroup.com/uk/en/cookie-policy">Cookies policy</a>
                  </li>
                  <li>
                      <a href="https://aviosgroup.com/uk/en/">aviosgroup.com</a>
                  </li>
              </ul>
          </nav>

          <p class="footer-copy">© 2018 Avios, All rights reserved.</p>
      </footer>


       
        <script src="scripts/app.js?ver=20180319_5"></script>
        <script src="scripts/cookie-message.js"></script>
        
    </body>
</html>
