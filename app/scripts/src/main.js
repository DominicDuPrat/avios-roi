  $(function () {
     'use strict';    
      $(document).ready(function(){
   
      
      // # SMOOTH SCROOLER
      $('a.js-smooth-scroller').on('click', function(event) {
        if (this.hash !== '') {
          event.preventDefault();
          var hash = this.hash;
          if ($(window).width() >= 768){
            $('html, body').animate({
               scrollTop: $(hash).offset().top - 74
              }, 800
            );
          }else{
            $('html, body').animate({
               scrollTop: $(hash).offset().top
              }, 800
            );
          }
          
        } // End if
      });


      // Add class to header when 10 pixels scrolled
      if ($(window).width() >= 768) {
          $(window).scroll(function() {    
              var scroll = $(window).scrollTop();

              if (scroll >= 10) {
                  $(".header").addClass("scrolled");
              } else {
                  $(".header").removeClass("scrolled");
              }
          });  
      } else {
          $('body').addClass('mobile');
      }


      $('#nav-icon').click(function(){
        $(this).toggleClass('open');
        $('.hiddenNavContainer').slideToggle('slow');
      });
          

      //COUNT TO
      function initCounters(){
          $('.js-count').appear(function(){
              var count = $(this);
              count.countTo({
                  from: 0,
                  to: count.html(),
                  speed: 2600,
                  refreshInterval: 60,
              });

          });
      }
      initCounters();


      //SVG FALLBACK TO PNG... just in case someone asks for IE8 support
      if (!Modernizr.svg) {
          $('img[src$=".svg"]').each(function()
          {
              $(this).attr('src', $(this).attr('src').replace('.svg', '.png'));
          });
      }

      

      $("#signup_form").submit(function(e) {

        var url = $(this).attr('action');

        $.ajax({
               type: "POST",
               url: url,
               data: $(this).serialize(),
               success: function(data)
               {
                 console.log('Submission was successful.');                 
                 window.location="thanks.php";
               }
             });

        e.preventDefault(); 

    });


      //close cookie bar
      $('#cookie-close').on('click', function(event) {
        event.preventDefault();
        $('#cookie-message').slideToggle();        
      });

      //init paraxify

      $(function(){
        if (document.documentElement.clientWidth > 720) {
            
            $('#section-hero').parallax({
                speed: '0.3',
                zIndex: '2',
                imageSrc: 'images/parallax/hero-top.jpg'
            });

            $('#loyalty-by-numbers').parallax({
                speed: '0.5',
                zIndex: '2',
                imageSrc: 'images/parallax/swim.jpg'
            });

            $('#section-quote').parallax({
                speed: '0.5',
                positionY: '10px',
                zIndex: '2',
                imageSrc: 'images/parallax/shopping.jpg'
            });

            $('#partnership-cta').parallax({
                speed: '0.5',
                zIndex: '2',
                imageSrc: 'images/parallax/boats.jpg'
            });
        }
    });

      
          
              
              

          
      
     


  }); // end of doc ready  
});

