<!doctype html>
<html class="no-js" lang="">
    <head>
   
        
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117334883-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117334883-1');
</script>




        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Avios Thought Leadership</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="styles/styles.css?ver=20180319_5">

        <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="icon" type="image/png" href="favicon.ico" />

    </head>
    <body>

        <div id="cookie-message">
            <p>Our website works best using cookies. By continuing to use the site, you agree to the use of cookies. Read more about our <a href="https://aviosgroup.com/uk/en/cookie-policy" class="u-text--white" target="_blank">Cookies</a> and <a href="https://aviosgroup.com/uk/en/privacy-policy" class="u-text--white" target="_blank">Privacy</a> notice.            
            <button id="cookie-close" class="cookie-close" data-js="true"><span class="visuallyhidden"> Accept</span></button>
        </div>

        
        <header class="header" id="header">
          <nav class="nav">
            
            <div class="mobileLogo">
                <a href="http://aviosgroup.com" target="_blank">
                    <img src="images/avios-logo.png" class="logo" alt="avios logo" />
                </a>
            </div>
            
            <div class="container hiddenNavContainer">
                
                    <ul>
                        <li>
                            <a href="#customer-loyalty" class="js-smooth-scroller">Value of customer loyalty</a>
                        </li>
                        <li class="borderRight">
                            <a href="#loyalty-by-numbers" class="js-smooth-scroller">Loyalty by numbers</a>
                        </li>
                        <li class="noBorder hiddenMobile">
                            <a href="http://aviosgroup.com" target="_blank">
                                <img src="images/avios-logo.png" class="logo" alt="avios logo" />
                            </a>
                        </li>
                        <li>
                            <a href="#increase-returns" class="js-smooth-scroller">Increase returns</a>
                        </li>
                        <li>
                            <a href="#section-whitepaper" class="js-smooth-scroller">Whitepaper</a>
                        </li>
                    </ul>
                
            </div>
            
            <div id="nav-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </nav>                     
          
      </header>

      <section id="section-hero">
          <div class="headerContent text-centered">
            <div class="container">
                <div class="">
                    <h1>Tapping into the value of customer loyalty</h1>
                   
                    
                    <div class="internalLink">
                        <a href="#section-whitepaper" class="button js-smooth-scroller">Download the whitepaper</a>
                    </div>
                      
                </div>
            </div>
            <div class="downArrow">                
                <a href="#section-paralax-first" class="js-smooth-scroller"><img src="images/arrow_down.png" alt="arrow-down"></a>
            </div>
        </div>
      </section>

      <section id="section-paralax-first">
          <h2>A well-executed loyalty programme <br>engages customers and enhances returns.</h2>
      </section>

      <section id="customer-loyalty">
          <h3>But how do you balance your loyalty gains<br>against your programme costs?</h3>
          <p>With over 30 years in the loyalty sector, we have a long history of delivering a strong return on our partners’ investment. Through a combination of tried-and-tested loyalty strategies, the backing of a global airline network and our aspirational travel rewards, we consistently improve customer loyalty and profitability across a breathtaking range of partnerships.</p>
      </section>

      <section id="loyalty-by-numbers">
        <div class="wrap-relative">
          <h4>LOYALTY BY NUMBERS</h4>
        </div>
      </section>

      <section id="loyalty">

            <p class="loyalty-title">Avios delivers tangible return on investment across a number of measures, ranging from customer acquisition and retention to spend and satisfaction.</p>

          <ul class="clear">
              <li>
                  <p class="loyalty-data"><span class="js-count">5</span>X</p>
                  <p class="loyalty-data-title">Basket spend</p>
                  <p class="loyalty-excerpt"><span class="excerpt-large">5x</span><span> more spend</span> by Avios collectors vs non-collectors<br> at a major shopping outlet</p>
              </li>
              <li>
                  <p class="loyalty-data"><span class="js-count">19</span>%</p>
                  <p class="loyalty-data-title">Visit frequency</p>
                  <p class="loyalty-excerpt"><span class="excerpt-large">19% </span><span>increase in visit frequency</span> at one everyday<br> spend partner after 6 months</p>
              </li>
              <li>
                  <p class="loyalty-data"><span class="js-count">9</span>%</p>
                  <p class="loyalty-data-title">Share of wallet</p>
                  <p class="loyalty-excerpt"><span class="excerpt-large">9% </span><span>increase in wallet share</span> at one food and beverage<br> partner after 6 months</p>
              </li>
          </ul>

      </section>


      <section id="increase-returns">
          <h5>GET MORE FROM YOUR LOYALTY INVESTMENT</h5>
      </section>


      <section id="increase-value">
          
          <p class="increase-title">Avios has commissioned an independent whitepaper exploring how to improve the value of your loyal customers, covering everything from reward strategies and benchmarking to programme costs and ROI theories.</p>

          <ul>
            <li>
                <img src="images/icons/heart.png" alt="heart">
                <p class="value-data-title">Boost customer value</p>
                <p class="value-excerpt">Make loyalty collection simple, <br>relevant and rewarding</p>
            </li>
            <li>
                <img src="images/icons/invest.png" alt="heart">
                <p class="value-data-title">Optimise investment</p>
                <p class="value-excerpt">Identify optimal campaign investment<br>with S-Curve theory</p>
            </li>
            <li>
                <img src="images/icons/bench.png" alt="bench">
                <p class="value-data-title">Benchmark success</p>
                <p class="value-excerpt">Keep track of the returns<br>your programme delivers</p>
            </li>
          </ul>
      </section>

      <section id="section-quote">
        <div class="quote-wrapper">
             <p>"Finding the right loyalty programme for your business, and proving its worth with accurate benchmarking, is key to long-lasting commercial success"<span>Chris Treadwell, Avios Commercial Director</span></p>
        </div>
         
      </section>

       <section id="section-whitepaper">
          <p class="section-title">Maximising the return on your loyalty investment</p>
          <p>When loyalty programmes are tailored to your brand and your target market they can be very profitable indeed. <br>However, such programmes come at a cost, and there’s a fine line between rewarding customers and adding value to your business. Rewarding too frequently can put a strain on your budget, while not rewarding enough can diminish the impact of your programme and its returns.</p>
          <br>
          <p class="strong">We have commissioned an independent whitepaper to pinpoint the variables and identify tangible ways to optimise the value of your loyalty investment.</p>
      </section>

      <section id="whitepaper-form">
       
        <form action="php/process.php" method="post" class="js-slideOut-onsuccess" id="signup_form">

             <p>Please fill in all the fields to download the whitepaper</p>
            
            <div class="row">
                <div class="two columns">
                    <label for="first-name">First name</label>
                </div>
                <div class="ten columns">
                    <input type="text" name="first-name" id="first-name" required />
                </div>
            </div>
            
            <div class="row">
                <div class="two columns">
                    <label for="surname">Surname</label>
                </div>
                <div class="ten columns">
                    <input type="text"  name="surname" id="surname" required />                              
                </div>
            </div>
            <div class="row">
                <div class="two columns">
                    <label for="company">Company</label>
                </div>
                <div class="ten columns">
                    <input type="text" name="company" id="company" required />
                </div>
            </div>
            <div class="row">
                <div class="two columns">
                    <label for="job-title">Job title</label>
                </div>
                <div class="ten columns">
                    <input type="text" name="job-title" id="job-title" required />
                </div>
            </div>
            <div class="row">
                <div class="two columns">
                    <label for="Email">Email</label>
                </div>
                <div class="ten columns">
                    <input type="email"  name="Email" id="Email" required />                             
                </div>
            </div>
            <div class="row">
                <div class="two columns">
                    <label for="telNo">Telephone</label>
                </div>
                <div class="ten columns">
                    <input type="tel"  name="telNo" id="telNo"/>                             
                </div>
            </div>
            <div class="row terms">
                <p>Terms and conditions / Privacy</p>
                <div class="tick">                    
                    <label><input type="checkbox" name="agree" value="1" id="agree" required /> I agree to receive promotional emails from Avios Group (AGL) Ltd about Avios products and services. I have read and agree to the <a href="https://aviosgroup.com/uk/en/terms">Terms</a> and <a href="https://www.aviosgroup.com/uk/en/privacy-policy">Privacy Policy</a>.</label>
                </div>
            </div>
            <div class="row text-centered">
                <input type="submit" class="button" id="send-form" value="Download whitepaper" />
            </div>
        </form>

        <div class="js-slideIn-onsuccess">
            <p class="success-title">Thank you for downloading our whitepaper</p>
            <p>To discover how Avios improves loyalty ROI by sector, choose from the following case studies:</p>
            <ul class="case-studies">
                <li>
                    <a href="https://aviosgroup.com/Avios_B2B_Hilton_Case_Study.pdf"><span><img src="images/hilton.jpg" alt="Hilton case study" /></span>Hilton</a>
                </li>
                <li>
                     <a href="https://aviosgroup.com/Avios_B2B_Kaligo_Case_Study.pdf"><span><img src="images/kaligo.jpg" alt="Hilton case study" /></span>Kaligo</a>
                </li>                    
                <li>
                    <a href="#"><span><img src="images/Bicester.jpg" alt="Hilton case study" /></span> Bicester Village Shopping Collection</a>
                </li>
            </ul>
            
            <p>To discuss how loyalty can maximise returns for your business, contact your Partnerships &amp; Business Development Manager:</p>
            
            <div class="contact-details">
                <p>Tel. : +44 (0) 7787 295 265  </p>
                <p>Email : <a href="mailto:ross.mitchell@avios.com">ross.mitchell@avios.com</a></p>
            </div>
            

            <a class="hidden" id="pdf-download" title="hello"></a>
        </div>

      </section>

      <section id="partnership-cta">
        <div class="container">
          <p class="section-title">Become an Avios partner</p>
          <p>We welcome enquiries from businesses interested in partnering with Avios. Please get in touch to see how we could work together to drive loyalty from your current and future customers.</p>
          <a href="mailto:partners@avios.com" target="_top">partners@avios.com</a> 
        </div>          
      </section>

      <section id="social-bar">
          <p>Connect with us</p>

          <a href="https://www.linkedin.com/company/avios" target="_blank"><img src="images/icons/linkedin.png" alt="Linkedin" /></a>
          <a href="https://www.youtube.com/user/AviosGlobal" target="_blank"><img src="images/icons/youtube.png" alt="youtube" /></a>
      </section>

      <footer>

        <div class="footer-logo">
                <a href="#">
                    <img src="images/avios-logo.png" class="logo" alt="avios logo" />
                </a>
        </div>
          
          <nav id="footer-nav">
              <ul>
                  <li>
                      <a href="https://www.aviosgroup.com/uk/en/privacy-policy">Privacy Policy</a>
                  </li>
                  <li>
                      <a href="https://www.aviosgroup.com/uk/en/accessibility">Accessibility</a>
                  </li>
                  <li>
                      <a href="https://aviosgroup.com/uk/en/terms">Terms &amp; conditions</a>
                  </li>
                  <li>
                      <a href="https://aviosgroup.com/uk/en/cookie-policy">Cookies policy</a>
                  </li>
                  <li>
                      <a href="https://aviosgroup.com/uk/en/">aviosgroup.com</a>
                  </li>
              </ul>
          </nav>

          <p class="footer-copy">© 2018 Avios, All rights reserved.</p>
      </footer>


       
        <script src="scripts/app.js?ver=20180319_5"></script>
        <script src="scripts/cookie-message.js"></script>
    </body>
</html>
